const express = require('express');
const app = express();
const morgan = require('morgan');
const func = require('./fn/function');
const router = require('./router');
const mongodb = require('./db/mongo');
const cors = require('cors');

mongodb.InitDbConnection();

app.use(cors({
    exposedHeaders: ['Authorization'],
    origin: '*'
}));
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', router);

app.listen(func.normalizePort(process.env.PORT || '3000'), () =>{
    console.log('server user initialized');
});

