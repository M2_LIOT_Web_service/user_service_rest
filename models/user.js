const mongoose = require("mongoose");
const crypto = require("../service/crypto");

var validateEmail = function(email) {
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email)
};

const UserSchema = new mongoose.Schema({
  _id: { type: String },
  userName: {
    type: String,
    trim: true,
    required: [true, "Le nom est obligatoire"],
  },
  firstName: {
    type: String,
    trim: true,
  },
  email: {
    type: String,
    trim: true,
    required: [true, "L'email est obligatoire"],
    lowercase: true,
    unique: true,
    validate: [validateEmail, 'Veuillez renseigner une adresse mail valide'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Veuillez renseigner une adresse mail valide']
  },
  password: {
    type: String,
    trim: true,
  },
  token: {
    type: String
  },
  data: {
    type: Object,
  },
});

UserSchema.plugin(require("mongoose-role"), {
  roles: ["public", "user", "admin"],
  accessLevels: {
    public: ["public", "user", "admin"],
    anon: ["public"],
    user: ["user", "admin"],
    admin: ["admin"],
  },
});

// On hash le mot de passe avant la sauvegarde d'un user
UserSchema.pre('save', function(next) {
    if (!this.isModified('password')) {
        return next();
    }
    this.password = crypto.encryptPassword(this.password);
    next();
});

module.exports = mongoose.model("User", UserSchema);

/**
 * Encrypte le mot de passe et l'affecte dans le modèle
 * @param {*} password
 */
// UserSchema.methods.setPassword = (password) =>{
//     //this.model("User").password = crypto.encryptPassword(password);
//       console.log('this ', this);
//       console.log('this.model ', this?.model);
//       console.log('module export : ', module.exports.password);
//   };
