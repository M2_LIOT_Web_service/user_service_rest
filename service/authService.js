const User = require("../models/user");
const jwt = require("jsonwebtoken");
const crypto = require("./crypto");

/**
 * Login. Renvoi un Token après vérification de l'existance de l'utilisateur
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
exports.login = async (req, res) => {
  const { email, password } = req.body;
  try {
    let user = await User.findOne({ email: email });
    if (user) {
      if (crypto.comparePassword(password, user.password)) {
        const token = jwt.sign(
          { id: user._id, name: user.name, firstName: user.firstName, role: user.role },
          process.env.TOKEN_SECRET_KEY,
          { expiresIn: "1h" }
        );
        // On stocke le token dans l'utilisateur
        user.token = token;
        // Sauvegarde du user avec le token
        user.save();
        res.header("Authorization", "Bearer " + token);
        return res.status(201).json({message: "Login Success", token : token, userName: user.userName, firstName: user.firstName });
      }
      return res.status(403).json("Wrong Credentials");
    } 
    else {
      return res.status(404).json("User not found");
    }
  } catch (err) {
    return res.status(500).json(err);
  }
};
