const CryptoJS = require("crypto-js");
const KEY = process.env.PASSWORD_CRYPT_KEY;

/**
 * Hash le mot de pass en paramètres et le compare à celui de la bdd
 * @param {*} password    le mot de passe à comparer
 * @param {*} passwordBdd le mot de passe qui se trouve en Bdd et déjà crypté
 * @returns
 */
exports.comparePassword = (password, passwordBdd) => {
  let hashPassword = module.exports.encryptPassword(password).toString();
  if (hashPassword === passwordBdd) {
    return true;
  }
  return false;
};

/**
 *
 * @param {*} password
 * @returns
 */
exports.encryptPassword = (password) => {
  return CryptoJS.HmacSHA512(password, KEY);
};
