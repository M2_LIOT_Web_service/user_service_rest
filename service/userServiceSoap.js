const User = require('../models/user');
const mongoose = require('mongoose');

const serviceSoap = {
    user_service : {
        user_port : {
            getUsers: (args, callback) => {
                User.find()
                .exec()
                .then(users => {
                    let userArray = [];
                    users.forEach(user => 
                        userArray.push({
                            _id: user._id,
                            email: user.email,
                            name: user.name,
                            firstName:user.firstName,
                            password: user.password,
                            data: user.data
                        }) 
                    );
                    callback({users: userArray})
                })
                .catch(err => callback(err));
            },

            getUserByEmail: (args, callback) => {
                User.findOne({email: args.email})
                .exec()
                .then(user => callback({user: {
                    _id: user._id,
                    email: user.email,
                    name: user.name,
                    firstName:user.firstName,
                    password: user.password,
                    data: user.data
                }}))
                .catch(err => callback(err));
            },

            addUser: (args, callback) => {
                const _id = mongoose.Types.ObjectId();
                const user = new User({ _id:_id , ...args.user});
                console.log("addUser args : ", args);
                console.log("addUser : ", user);
                user.save((err, user)=>{
                    if(err){
                        callback(err);
                    } else {
                        callback({ user: {
                            _id: user._id,
                            email: user.email,
                            name: user.name,
                            firstName:user.firstName,
                            password: user.password,
                            data: user.data
                        } });
                    }
                });
            },

            updateUser: (args, callback)  => {
                const filter = {email: args.user.email};
                User.findOneAndUpdate(filter, args.user)
                .then(user => User.findOne(filter)
                    .then(userUpdate => callback({user: {
                        _id: userUpdate._id,
                        email: userUpdate.email,
                        name: userUpdate.name,
                        firstName:userUpdate.firstName,
                        password: userUpdate.password,
                        data: userUpdate.data
                    }}))
                    .catch(err2 => callback(err2))
                )
                .catch(err => callback(err));
            },

            deleteUser: (args, callback) => {
                User.findOneAndDelete({email: args.email})
                .then(res => callback({message: res}))
                .catch(err => callback({message: err}));
            }

        }
    }
}

module.exports = serviceSoap;