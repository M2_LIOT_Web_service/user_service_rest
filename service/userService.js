const User = require("../models/user");
const mongoose = require("mongoose");
const security = require("../service/security");

/**
 * Get User by his id
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.getById = async (req, res) => {
  const id = req.params.id;
  try {
    // On retourne l'utilisateur avec l'id donné mais on ne renvoi pas le password et le token { password: 0, token: 0 }
    let user = await User.findById(id, { password: 0, token: 0 });
    if (user && user._id === id) {
      return res.status(200).json(user);
    }
    return res.status(404).json("User not found");
  } catch (err) {
    return res.status(500).json(err);
  }
};

/**
 * Get Users by his id or by mail if email in path parameters apiUrl/users?email=mail@gmail.com
 * @param {*} req
 * @param {*} res
 */
exports.getUsers = (req, res) => {
  let filter = {};
  if (req.query.email) {
    filter = { email: req.query.email };
  }

  User.find(filter, { password: 0, token: 0 })
    .exec()
    .then((users) => {
      // On filtre pour afficher seulement les ressources dont l'utilisateur a accès
      // console.log("getUsers users : ", users);
      // const userList = users?.filter(user => security.hasAccessRessource(header, user._id) == true );
      // console.log("getUsers userList : ", userList);
      // if (userList && userList.length > 0) {
      //   return res.status(200).json(userList);
      // } else {
      //   return res.status(204).json("Not Users in database");
      // }
      if(users && users.length > 0) {
        return res.status(200).json(users);
      }
      return res.status(204).json("Not Users in database");
    })
    .catch((err) =>
      res.status(500).json({
        message: "Server error",
        error: err,
      })
    );
};

/**
 * Add users to the database
 * @param {*} req
 * @param {*} res
 */
exports.add = async (req, res) => {
  const _id = mongoose.Types.ObjectId();
  const user = new User({ _id: _id, role: "user", ...req.body });
  user.save(
    (err) => {
      if (err) {
        console.log(err);
        if (err.code === "11000" || err.code === 11000) {
          return res.status(409).json("This Email Address already exists");
        }
        return res.status(500).json(err);
      }
      return res.status(201).json(user);
    },
    { password: 0, token: 0 }
  );
};

/**
 * Update users to the database
 * @param {*} req
 * @param {*} res
 */
exports.update = async (req, res) => {
  try{
    const user = await User.findOne({ email: req.body.email });
    if(user && security.hasAccessRessource(req.header("Authorization"), user._id)){
      User.findOneAndUpdate({ email: req.body.email }, req.body)
      .then((user) =>
        User.findOne({ email: req.body.email }, { password: 0, token: 0 })
          .then((newUser) => res.status(200).json(newUser))
          .catch((err2) => res.status(500).json(err2))
      )
      .catch((err) => res.status(500).json(err));
    }
  } catch(err){
    return res.status(500).json(err);
  }
};

/**
 * Delete user by his id.
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.delete = async (req, res) => {
  const id = req.params.id;
  try {
    // On vérifie que l'utilisateur loggé puisse delete l'utilisateur envoyé dans la requête
    if(security.hasAccessRessource(req.header("Authorization"), id)) {
      const result = await User.findByIdAndDelete(id);
      if (result && result._id) {
        return res
        .status(200)
        .json(`User ${result.userName} ${result.firstName} has been deleted`);
      }
      return res.status(404).json("Request failed ! User not found.");
    }
  } catch (err) {
    return res.status(500).json(err);
  }
  return res.status(403).json("Forbidden with your user role");
};
