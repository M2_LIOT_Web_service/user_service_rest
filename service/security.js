const jwt = require("jsonwebtoken");
const TOKEN_KEY = process.env.TOKEN_SECRET_KEY;
const User = require("../models/user");

/**
 * Décode le token
 * @param {*} token
 */
exports.decodeToken = (token) => {
  return jwt.decode(token, { complete: true });
};

/**
 * Renvoi le token s'il existe dans le headers
 * @param {*} headers request headers
 * @returns
 */
exports.getTokenFromHeaders = (authorizationHeader) => {
  let token;
  if (!!authorizationHeader && authorizationHeader.startsWith("Bearer ")) {
    token = authorizationHeader.slice(7, authorizationHeader.length);
  }
  return token;
};

/**
 * Méthode de vérification du token avant envoie vers la requête désirée
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.verifyToken = (accessLevel) => {
  return (req, res, next) => {
    let token = module.exports.getTokenFromHeaders(req.header("Authorization"));
    if (token) {
      jwt.verify(token, TOKEN_KEY, (err, decoded) => {
        if (err) {
          return res.status(498).json("Token is not valid");
        } else {
          // Récupération du user avec l'id passé dans le token
          User.findById(decoded.id)
            .then((user) => {
              req.decoded = decoded;
              // On vérifie si le jeton envoyé est celui stocké dans la bdd
              if (token !== user.token) {
                return res
                  .status(498)
                  .json(
                    "Your token is expired"
                  );
              }

              // On renvoie un token pour mettre à jour le end time
              const newToken = jwt.sign(
                {
                  id: decoded.id,
                  name: decoded.name,
                  firstName: decoded.firstName,
                  role: decoded.role,
                },
                TOKEN_KEY,
                { expiresIn: "1h" }
              );
              // On change le token de l'utilisateur. Permet de rester connecté tant qu'il y a de l'activité
              user.token = newToken;
              // Sauvegarde du token en base de données
              user.save();
              res.header("Authorization", "Bearer " + newToken);
              console.log("new token : ", newToken);

              // On vérifie que l'utilisateur possède les droits requit pour cette requête
              if (!user.hasAccess(accessLevel)) {
                return res
                  .status(403)
                  .json(
                    "Forbidden Acces with your role"
                  );
              }
              next();
            })
            .catch((err) => res.status(500).json(err));
        }
      });
    } else {
      return res.status(401).json("Authentication required");
    }
  };
};

exports.hasAccessRessource = (header, userId) => {
  const token = module.exports.getTokenFromHeaders(header);
  const payload = module.exports.decodeToken(token).payload;
  // On vérifie avec le token que l'utilisateur puisse accéder à la ressource
  if (
    payload.role == "admin" ||
    (payload.id == userId && payload.role == "user")
  ) {
    return true;
  }
  return false;
};
