const express = require("express");
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
// eslint-disable-next-line new-cap
const router = express.Router();
const userService = require("./service/userService");
const authService = require('./service/authService');
const security = require("./service/security");

/**
 * Définition des options Swagger
 */
//#region swagger
const swaggerOptions = {
    definition: {
      openapi: "3.0.0",
      info: {
        title: "User Rest API",
        version: "1.0.0",
        description: "Simple User management API.",
        contact: {
          name: "Les ambassadeurs",
        },
        servers: ["http://localhost:8000"],
      },
      components: {
        securitySchemes: {
          jwt: {
            type: "http",
            scheme: "bearer",
            in: "header",
            bearerFormat: "JWT"
          },
        },
      },
      security: { jwt: [] },
    },
    apis: ["router.js"], // files containing annotations as above
  };
  
  const swaggerDocs = swaggerJsdoc(swaggerOptions);
  //#endregion

  router.use("/api/v1/users/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

  /**
 * @openapi
 * /api/v1/login:
 *   post:
 *     description: Use to get auth token
 *     tags:
 *       - Authentication
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - email
 *               - password
 *             properties:
 *              email:
 *                type: string
 *              password:
 *                type: string
 *     responses:
 *       201:
 *         description: {message : "Login Success", token, userName, firstName}
 *       401:
 *         description: Authentication required
 *       403:
 *         description: Forbidden Acces with your role / Wrong Credentials
 *       404:
 *         description: User not found
 *       498:
 *         description: Token is not valid / Your token is expired
 *       500:
 *         description: Internal Error
 */
router.post('/api/v1/login', authService.login);

/**
 * @openapi
 * /api/v1/users:
 *   get:
 *     description: Returns all users in database.
 *     tags:
 *       - Users
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: email
 *         type: string
 *         description: If email (users?email=johndoe@gmail.com) parameters then you search only the User with this address email.
 *     security:
 *       - jwt: []
 *     responses:
 *       200:
 *         description: Returns (JSON) all users in database.
 *       204:
 *         description: Not Users in database.
 *       401:
 *         description: Authentication required
 *       403:
 *         description: Forbidden Acces with your role / Wrong Credentials
 *       498:
 *         description: Token is not valid / Your token is expired
 *       500:
 *         description: Internal Server Error
 */
router.get("/api/v1/users", security.verifyToken('user') , userService.getUsers);

// Si url / renvoie vers /users
router.use("/", (req, res, next) => {
  if (req.url == "/") {
    res.redirect("/api/v1/users/api-docs");
  } else {
    next();
  }
});

/**
 * @openapi
 * /api/v1/users:
 *   post:
 *     description: Add User in database.
 *     tags:
 *       - Users
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: userName and email are required. email must to be unique in database.
 *             type: object
 *             required:
 *               - userName
 *               - email
 *             properties:
 *               userName:
 *                 type: string
 *               firstName:
 *                 type: string
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *               data:
 *                 type: object
 *     responses:
 *       201:
 *         description: Return (JSON) new User.
 *       409:
 *         description: This Email Address already exists
 *       500:
 *         description: Internal Server Error
 */
router.post("/api/v1/users", userService.add);

/**
 * @openapi
 * /api/v1/users:
 *   put:
 *     description: Modify an User in database.
 *     tags:
 *       - Users
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: userName and email are required. email must to be unique in database.
 *             type: object
 *             required:
 *               - userName
 *               - email
 *             properties:
 *               userName:
 *                 type: string
 *               firstName:
 *                 type: string
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *               data:
 *                 type: object
 *     security:
 *       - jwt: []
 *     responses:
 *       200:
 *         description: Return (JSON) an user by his email.
 *       401:
 *         description: Authentication required
 *       403:
 *         description: Forbidden Acces with your role / Wrong Credentials
 *       498:
 *         description: Token is not valid / Your token is expired
 *       500:
 *         description: Internal Server Error
 */
router.put("/api/v1/users", security.verifyToken('user'), userService.update);

/**
 * @openapi
 * /api/v1/users/{id}:
 *   get:
 *     description: Get user by his id.
 *     tags:
 *       - Users
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *     security:
 *       - jwt: []
 *     responses:
 *       200:
 *         description: Return (JSON) an user by his id.
 *       401:
 *         description: Authentication required
 *       403:
 *         description: Forbidden Acces with your role / Wrong Credentials
 *       404:
 *         description: User Not Found.
 *       498:
 *         description: Token is not valid / Your token is expired
 *       500:
 *         description: Internal Server Error
 */
 router.get("/api/v1/users/:id", security.verifyToken('user'), userService.getById);

 /**
 * @openapi
 * /api/v1/users/{id}:
 *   delete:
 *     description: Delete user by his id.
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: id
 *     security:
 *       - jwt: []
 *     responses:
 *       200:
 *         description: Delete an user by his id.
 *       401:
 *         description: Your token is expired
 *       403:
 *         description: Forbidden Acces with your role
 *       404:
 *         description: User Not Found.
 *       500:
 *         description: Internal Server Error
 */
 router.delete("/api/v1/users/:id", security.verifyToken('user'), userService.delete);

router.use((req, res) => {
  res.status(404).send("Page non trouvée");
});

module.exports = router;
